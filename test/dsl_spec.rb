require 'rspec'

require 'cdgraph'

RSpec.describe 'DSL' do

  context 'roles' do
    it 'recognizes roles with labels' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        role :test_role,
          label: 'Test Role'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.roles.length).to eq(1)
      expect(parsed.roles.first.id).to eq(:test_role)
      expect(parsed.roles.first.label).to eq('Test Role')
    end

    it 'can add definitions to a role with the same id' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        role :test_role
        role :test_role, label: 'Test Role'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.roles.length).to eq(1)
      expect(parsed.roles.first.id).to eq(:test_role)
      expect(parsed.roles.first.label).to eq('Test Role')
    end
  end

  context 'tasks' do
    it 'recognizes tasks with labels' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        task :test_task,
          label: 'Test Task'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.tasks.length).to eq(1)
      expect(parsed.tasks.first.id).to eq(:test_task)
      expect(parsed.tasks.first.label).to eq('Test Task')
    end

    it 'can add definitions to a task with the same id' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        task :test_task
        task :test_task, label: 'Test Task'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.tasks.length).to eq(1)
      expect(parsed.tasks.first.id).to eq(:test_task)
      expect(parsed.tasks.first.label).to eq('Test Task')
    end
  end

  context 'tasks and roles' do
    it 'can associate tasks with roles' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        role :a_role

        task :first_task, label: 'First Task', uses_roles: [:a_role]
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.tasks.first.uses_roles).to include(:a_role)
      expect(parsed.roles.first.id).to eq(:a_role)
    end
  end

  context 'nodes' do
    it 'recognizes nodes with labels' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        node :test_node,
          label: 'Test Node'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.nodes.length).to eq(1)
      expect(parsed.nodes.first.id).to eq(:test_node)
      expect(parsed.nodes.first.label).to eq('Test Node')
    end

    it 'can add definitions to a node with the same id' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        node :test_node
        node :test_node, label: 'Test Node'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.nodes.length).to eq(1)
      expect(parsed.nodes.first.id).to eq(:test_node)
      expect(parsed.nodes.first.label).to eq('Test Node')
    end
  end


  context 'environments' do
    it 'recognizes environments with labels' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        environment :test_environment,
          label: 'Test Environment'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.environments.length).to eq(1)
      expect(parsed.environments.first.id).to eq(:test_environment)
      expect(parsed.environments.first.label).to eq('Test Environment')
    end

    it 'can add definitions to a environment with the same id' do
      src =<<-END.gsub(/^[ ]{8}/, '')
        environment :test_environment
        environment :test_environment, label: 'Test Environment'
      END

      parsed = CDGraph::DSL.parse(src, file: __FILE__, line: __LINE__)

      expect(parsed.environments.length).to eq(1)
      expect(parsed.environments.first.id).to eq(:test_environment)
      expect(parsed.environments.first.label).to eq('Test Environment')
    end
  end
end