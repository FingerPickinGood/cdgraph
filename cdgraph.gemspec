# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cdgraph/version'

Gem::Specification.new do |spec|
  spec.name          = 'cdgraph'
  spec.version       = CDGraph::VERSION
  spec.authors       = ['Tristan Juricek']
  spec.email         = ['mr.tristan@gmail.com']
  spec.summary       = %q{CD Pipeline Graph Generator}
  spec.description   = %q{A basic DSL for generating Graphviz graphs of continous delivery pipelines.}
  spec.homepage      = 'http://tristanjuricek.com/cdgraph'
  spec.license       = 'MIT'

  spec.files         = Dir.glob('bin/**/*') +
    Dir.glob('docsrc/**/*') +
    Dir.glob('lib/**/*') +
    Dir.glob('test/**/*') +
    %w(
      cdgraph.gemspec
      Gemfile
      Gemfile.lock
      LICENSE.txt
      Rakefile
      Readme.md
    )
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.1'
end
