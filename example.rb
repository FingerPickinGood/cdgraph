
task :build_app, label: "Build 'theapp'"

role :automake
role :emacs
role :gcc, label: 'GCC 4.7'
role :gdb
role :git
role :git_server, global: true
role :vim

task :checkout_source, uses_roles: [:git, :git_server]
task :checkin_source, uses_roles: [:git, :git_server]
task :build_app, uses_roles: [:gcc, :automake], depends_on: [:checkout_source]
task :debug, uses_roles: [:gdb], depends_on: [:build_app]
task :edit, uses_roles: [:emacs, :vim]

node :dev_ubuntu14,
     label: 'Ubuntu 14 (Dev)',
     provides_roles: [:gcc, :automake, :git, :emacs, :vim, :gdb],
     runs_tasks: [:checkout_source, :edit, :build_app, :debug, :checkin_source]

node :build_ubuntu14,
     label: 'Ubuntu 14 (Build)',
     provides_roles: [:gcc, :automake, :git],
     runs_tasks: [:checkout_source, :build_app]

node :scm,
     label: 'Git Server',
     provides_roles: [:git_server]

environment :build, label: 'Build', has_nodes: [:scm, :build_ubuntu14]
environment :dev, label: 'Development', has_nodes: [:dev_ubuntu14]

