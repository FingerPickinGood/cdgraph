require 'cdgraph/dsl'
require 'cdgraph/version'
require 'cdgraph/writer'

require 'tempfile'

class CDGraph

  def self.to_png(src, output)
    gv = to_graphviz(src)
    puts "gv\n#{gv}"
    graphviz_to_png(gv, output)
  end

  def self.to_graphviz(src, *options)
    dsl = DSL.parse(src, *options)
    writer = Writer.new(dsl: dsl)
    writer.render
  end

  def self.graphviz_to_png(graphviz_src, output)
    file = Tempfile.new(['cdgraph', '.dot'])

    dot_ok = false
    begin
      file.write(graphviz_src)
      file.close
      dot_ok = system "dot -Tpng -o #{output} #{file.path}"
    ensure
      file.close!
    end

    dot_ok
  end
end
