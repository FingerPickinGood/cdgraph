require 'erb'

class CDGraph
  class Writer < ERB

    attr_accessor :title
    attr_accessor :dpi
    attr_accessor :fontname
    attr_accessor :fontsize
    attr_accessor :template
    attr_accessor :dsl

    def initialize(title: nil, dpi: nil, fontname: nil, fontsize: nil, dsl: nil)
      @title = title
      @dpi = dpi or 75
      @fontname = fontname || 'Helvetica Neue'
      @fontsize = fontsize || 13
      @template = init_template
      @dsl = dsl
    end

    def render
      ERB.new(template).result(binding)
    end

    def item_id(env, node, item)
      unless item.global
        "#{env.id}_#{node.id}_#{item.id}"
      else
        item.id
      end
    end

    def init_template
      <<-END.gsub(/^[ ]{8}/,'')
        digraph G {
          graph [compound=true];
          graph [fontname="<%= fontname %>",fontsize=<%= fontsize %>];
          node [shape=record,fontname="<%= fontname %>",fontsize=<%= fontsize %>];
          edge [fontname="<%= @fontname %>",fontsize=<%= fontsize %>];

          graph[style="filled"];

          <% for @env in dsl.environments %>
          subgraph cluster_<%= @env.id %> {
            style="filled";
            color="#FFEEAD";

            <% for @node in dsl.nodes_for(@env) %>
            subgraph cluster_<%= @node.id %> {
              color="#96CEB4";
              style="filled";

              <% for @role in dsl.roles_for(@node) %>
                <%= item_id(@env, @node, @role) %> [label="<%= @role.label %>",style="filled",color="#FFCC5C"];
              <% end %>

              <% for @task in dsl.tasks_for(@node) %>
              <%= item_id(@env, @node, @task) %> [label="<%= @task.label %>",style="filled",color="#AFACD8"];
              <% end %>

              <% for @task in dsl.tasks_for(@node) %>
                <% for @dep in @task.depends_on %>
                  <%= item_id(@env, @node, @task) %> -> <%= item_id(@env, @node, dsl.find_task(@dep)) %>;
                <% end %>
                <%for @role in @task.uses_roles %>
                  <%= item_id(@env, @node, @task) %> -> <%= item_id(@env, @node, dsl.find_role(@role)) %> [style=dotted,arrowhead=none];
                <% end %>
              <% end %>

              label="<%= @node.label %>";
            }
            <% end %>

            label="<%= @env.label %>";
          }
          <% end %>
        }
      END
    end
  end
end