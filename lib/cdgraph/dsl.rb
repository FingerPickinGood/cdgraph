class CDGraph
  class DSL

    attr_accessor :tasks
    attr_accessor :roles
    attr_accessor :nodes
    attr_accessor :environments

    def initialize
      @tasks = []
      @roles = []
      @nodes = []
      @environments = []
    end

    def self.parse(src, file: nil, line: nil)
      dsl = DSL.new
      dsl.instance_eval(src, file || __FILE__, line || __LINE__)
      dsl
    end

    def role(id, *options)
      check_or_append(@roles, Role, id, *options)
    end

    def task(id, *options)
      check_or_append(@tasks, Task, id, *options)
    end

    def node(id, *options)
      check_or_append(@nodes, Node, id, *options)
    end

    def environment(id, *options)
      check_or_append(@environments, Environment, id, *options)
    end

    def nodes_for(env)
      env.has_nodes.map do |id|
        @nodes.find { |n| n.id == id }
      end
    end

    def roles_for(node)
      node.provides_roles.map do |id|
        @roles.find { |r| r.id == id }
      end
    end

    def tasks_for(node)
      node.runs_tasks.map do |id|
        @tasks.find { |t| t.id == id }
      end
    end

    def find_task(task_id)
      @tasks.find { |t| t.id == task_id }
    end

    def find_role(role_id)
      @roles.find { |r| r.id == role_id }
    end

    def get_binding
      binding()
    end

    private

    def check_or_append(arr, type, id, *options)
      item = arr.find{ |r| r.id == id }
      if item
        options.each do |h|
          h.each do |key, value|
            assign = "#{key}=".to_sym
            if item.respond_to?(assign)
              item.send(assign, value)
            end
          end
        end
      else
        args = {id: id}
        args = args.merge(*options) unless options.empty?
        arr << type.new(args)
      end
    end
  end

  class Task
    attr_accessor :id
    attr_accessor :label
    attr_accessor :depends_on
    attr_accessor :uses_roles
    attr_accessor :global

    def initialize(id: nil, label: nil, depends_on: nil, uses_roles: nil,
                   global: false)
      @id = id
      @label = label || id.to_s
      @depends_on= []
      @depends_on.concat(depends_on) if depends_on
      @uses_roles = []
      @uses_roles.concat(uses_roles) if uses_roles
      @global = global
    end

    def depends_on(*tasks)
      @depends_on.concat(tasks) if tasks
    end

    def uses_roles(*roles)
      @uses_roles.concat(roles) if roles
    end
  end

  class Role
    attr_accessor :id
    attr_accessor :label
    attr_accessor :node_ids
    attr_accessor :global

    def initialize(id: nil, label: nil, runs_on_nodes: nil, global: false)
      @id = id
      @label = label || id.to_s
      @node_ids = []
      @node_ids.concat(runs_on_nodes) if runs_on_nodes
      @global = global
    end

    def runs_on_nodes(*ary)
      @node_ids.concat(ary)
    end
  end

  class Node
    attr_accessor :id
    attr_accessor :label
    attr_accessor :runs_tasks
    attr_accessor :provides_roles

    def initialize(id: nil, label: nil, runs_tasks: nil, provides_roles: nil)
      @id = id
      @label = label || id.to_s
      @runs_tasks = []
      @runs_tasks.concat(runs_tasks) if runs_tasks
      @provides_roles = []
      @provides_roles.concat(provides_roles) if provides_roles
    end

    def runs_tasks(*tasks)
      @runs_tasks.concat(tasks)
    end

    def provides_roles(*roles)
      @provides_roles.concat(roles)
    end
  end

  class Environment
    attr_accessor :id
    attr_accessor :label
    attr_accessor :has_nodes

    def initialize(id: nil, label: nil, has_nodes: nil)
      @id = id
      @label = label || id.to_s
      @has_nodes = []
      @has_nodes.concat(has_nodes) if has_nodes
    end

    def has_nodes(*nodes)
      @has_nodes.concat(nodes)
    end
  end
end